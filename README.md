## Software for Estimating Number of Connected Graph Components in Sublinear Time
by Saeid Saadati Jafarabadi and Tõnis Ojandu
### Building

Building the software requires C++ compiler and [Allegro](https://wiki.allegro.cc/index.php?title=Getting_Started)
to be installed. 

* Edit build.sh so that it can access Allegro libraries when building
* Run:
```
./build.sh
```

### Running

Building will produce two binaries:
```
gen
```
and
```
comp
```

#### Gen

Gen is random graph generator. It is meant to produce input graph for graph
connected component number estimation. It will print the graph data into
standard output.

Usage:
```
./gen [number of nodes] [number of connected components] [edge probability] [width] [height] 
```
where:

* number of nodes: number of nodes in the graph to be generated
* number of connected components: number of connected components in the graph to be generated
* edge probability (between 0.0 and 1.0): defines the probability that an edge inside a component is included int the graph generated
* width: defines the width of the 2D plane for visualization during the estimation
* height: defines the height of the 2D plane for visualization during the estimation

#### Comp

Actual program that does the number of components estimation. It reads the graph data from 
standard input.

Usage without visualization:
```
./comp [epsilon]
```
and usage with visualization:
```
./comp [epsilon] v
```
where:

* epsilon: is the epsilon used in the estimation

### Graph data specification

This is the specification for graph data printed into the standard output from "gen" program and read from standard input by "comp":

* First line:
```
[number of nodes] [number of components]
```

* Following lines are either:

1) node definitions
```
n [index] [x coordinate] [y coordinate]
```
Note that node indicis ared integers and start from 0 and index for each node being defined must 1 + index of the last node defined

2) edge definitions
```
e [index1] [index2]
```
This defines a undirected edge between nodes with indices "index1" and "index2". Note that nodes with indices "index1" and "index2" will have to have been defined before this definition line.

* Final line
```
end
```

### Combined Usage

For testing it is possible to combine the commands. For example:
```
./build.sh && ./gen 1000 30 0.001 500 500 | ./comp 0.1 v
```