#!/bin/bash

width=600
height=600

../gen 1000 30 0.001 $width $height > gr3co30pr3.txt
../gen 10000 300 0.001 $width $height > gr4co300pr3.txt
../gen 100000 3000 0.001 $width $height > gr5co3000pr3.txt
../gen 1000000 30000 0.001 $width $height > gr6co30000pr3.txt
