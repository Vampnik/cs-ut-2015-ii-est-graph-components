## Pregenerated graphs

### Regeneration

Alter create.sh according to your needs. Width and height can be also changed in create.sh.

Usage:
```
./create.sh
```

### Name explaination

Graphs are named as following:
```
gr${exponent1}co${components}pr${exponent2}.txt
```
where:

* exponent1: determines the number of nodes in graph as 10^exponent1
* components: number of components
* exponent2: determines the probability (10^(-exponent2)) that was used when including edges

### Running

In order to test these graphs "comp" program has to have been built (see root README.md).

Example usage without visualization:
```
cat gr6co30000pr3.txt | ../comp 0.01
```
and example usage with visualization:
```
cat gr6co30000pr3.txt | ../comp 0.01 v
```