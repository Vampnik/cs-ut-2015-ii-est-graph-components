#!/bin/bash

g++ -o gen generator.cpp
g++ -o comp components.cpp -std=c++11 -lallegro -lallegro_primitives -lallegro_font -lallegro_main -lallegro_memfile -lallegro_color -lpthread