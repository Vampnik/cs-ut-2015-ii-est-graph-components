#include <iostream>
#include <sstream>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <stdexcept>
#include <math.h>
#include <algorithm>

using namespace std;

int r(int from, int to) {
    return from + (rand() % (to - from));
}

int i(double d) {
    return (int)d;
}

double d(int l) {
    return (double)l;
}

void generate(double prob, int fromX, int fromY, int toX, int toY, int from, int to, int components, bool splitX) {
    if(components > 1) {
        int mid = r(from, to);
        double pc = d(mid - from) / d(to - from);
        int first = max(1, i(components * pc));

        first = mid-from < first ? mid-from : first;
        first = to-mid < components-first ? components - (to-mid) : first;;

        if(splitX) {
            int midX = i((toX - fromX) * pc + fromX);
            generate(prob, fromX, fromY, midX-fromX == 0 ? midX+1 : midX, toY, from, mid, first, !splitX);
            generate(prob, toX-midX == 0 ? midX-1 : midX, fromY, toX, toY, mid, to, components-first, !splitX);
        } else {
            int midY = i((toY - fromY) * pc + fromY);
            generate(prob, fromX, fromY, toX, midY-midY==0 ? midY+1 : midY, from, mid, first, !splitX);
            generate(prob, fromX, toX-midY == 0 ? midY-1 : midY, toX, toY, mid, to, components-first, !splitX);
        }
    } else {
        int tree[to-from];
        int len = to - from;
        for(int i=from;i<to;i++) {
            cout << "n " << i << " " << r(fromX, toX) << " " << r(fromY, toY) << endl;
            if(i > from) {
                int conn = r(from, i);
                tree[i-from] = conn-from;
                cout << "e " << conn << " " << i << endl;
            }
        }
        for(int i=from;i<to;i++) {
            for(int j=i+1;j<to;j++) {
                double r = ((double)rand() / (double)(RAND_MAX));
                if(r < prob && tree[j-from] != i-from) {
                    cout << "e " << i << " " << j << endl;
                }
            }
        }
    }
}

int main(int argc, char* argv[]) {
    srand(std::time(0)%65537);

    int numNodes;
    int numComponents;
    double prob;
    int width;
    int height;


    if(argc < 6) {
        cerr << "Not enough arguments" << endl;
        return 1;
    }
    istringstream ss1(argv[1]);
    if (!(ss1 >> numNodes)) {
        cerr << "Invalid number of nodes: " << argv[1] << endl;
        return 1;
    }
    istringstream ss2(argv[2]);
    if (!(ss2 >> numComponents)) {
        cerr << "Invalid number of components: " << argv[2] << endl;
        return 1;
    }
    istringstream ss3(argv[3]);
    if (!(ss3 >> prob)) {
        cerr << "Invalid probability number: " << argv[3] << endl;
        return 1;
    }
    istringstream ss4(argv[4]);
    if (!(ss4 >> width)) {
        cerr << "Invalid width: " << argv[4] << endl;
        return 1;
    }
    istringstream ss5(argv[5]);
    if (!(ss5 >> height)) {
        cerr << "Invalid height: " << argv[5] << endl;
        return 1;
    }
    if (prob < 0.0 || prob > 1.0) {
        cerr << "Probability not between 0 and 1" << endl;
        return 1;
    }

    cout << numNodes << " " << numComponents << " " << width << " " << height << endl;
    generate(prob, 0, 0, width, height, 0, numNodes, numComponents, true);
    cout << "end" << endl;
}