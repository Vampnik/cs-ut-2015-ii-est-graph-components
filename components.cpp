#include <iostream>
#include <math.h>
#include <vector>
#include <set>
#include <string>
#include <sstream>
#include <algorithm>
#include <fstream>
#include <string>
#include <tr1/unordered_set>

#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_color.h>

using namespace std;

ALLEGRO_DISPLAY*    display;
ALLEGRO_EVENT_QUEUE* event_queue;
ALLEGRO_EVENT       event;

void init(int width, int height);
void deinit();
void event_loop();
void draw();
void drawTriangle(int x, int y);

struct edge {
  int a;
  int b;
};

int traverse(int index, vector< vector<int> * > *nodes, tr1::unordered_set<int> *visited, int steps, int limit, int coords[][2], ALLEGRO_COLOR color, bool visual) {
    visited->insert(index);
    steps++;
    if(steps > limit) {
        return steps;
    }
    for(int i=0;i<nodes->at(index)->size();i++) {
        int newIndex = nodes->at(index)->at(i);
        if(visited->find(newIndex) == visited->end()) {
            if(visual) {
                al_draw_line(coords[index][0], coords[index][1], coords[newIndex][0], coords[newIndex][1], color, 1);
                al_flip_display();
            }
            steps = traverse(newIndex, nodes, visited, steps, limit, coords, color, visual);
            if(steps > limit) {
                return steps;
            }
        }
    }
    return steps;
}

int main(int argc, char* argv[]) {
    srand(time(NULL));

    double epsilon;
    bool visual = false;
    string sVisual;

    if(argc < 2) {
        cerr << "Not enough arguments" << endl;
        return 1;
    }
    istringstream ss1(argv[1]);
    if (!(ss1 >> epsilon)) {
        cerr << "Invalid epsilon: " << argv[1] << endl;
        return 1;
    }
    if (epsilon < 0.0 || epsilon > 1.0) {
        cerr << "Epsilon not between 0 and 1" << endl;
        return 1;
    }
    if (argc > 2) {
        istringstream ss2(argv[2]);
        if(!(ss2 >> sVisual)) {
            cerr << "Invalid number of components: " << argv[2] << endl;
            return 1;
        }
        if(sVisual.compare("v") == 0) {
            visual = true;
        }
    }

    int numNodes;
    int numComponents;
    int width;
    int height;
    cin >> numNodes >> numComponents >> width >> height;

    if(visual) {
        init(width, height);
    }

    int coords[numNodes][2];
    vector<edge> edges;
    vector< vector<int> * > *nodes = new vector< vector<int> * >();

    ALLEGRO_COLOR black = al_map_rgb(0, 0, 0);

    string marker;
    int num, x, y, a, b;
    while(true) {
        cin >> marker;
        if(marker.compare("n") == 0) {
            cin >> num >> x >> y;
            coords[num][0] = x;
            coords[num][1] = y;
            nodes->push_back(new vector<int>());
        } else if(marker.compare("e") == 0) {
            cin >> a >> b;
            edge e;
            e.a = a;
            e.b = b;
            edges.push_back(e);
            nodes->at(a)->push_back(b);
            nodes->at(b)->push_back(a);
            if(visual) {
                al_draw_line(coords[e.a][0], coords[e.a][1], coords[e.b][0], coords[e.b][1], black, 1);
            }
        } else if(marker.compare("end") == 0) {
            break;
        } else {
            cerr << "Unknown marker: " << marker << endl;
            return 1;
        }
    }

    if(visual) {
        al_flip_display();
    }

    cout << "Using epsilon: " << epsilon << endl;

    int times = (int)(1.0 / epsilon / epsilon);
    int limit = (int)(2.0 / epsilon);

    double sum = 0.0;


    int logAt = (int)(times * 0.1);
    for(int t=0;t<times;t++) {
        if(logAt != 0 && t % logAt == 0) {
            cout << (100.0 * t / times) << "% " << t << "/" << times << endl;
        }
        int index = rand() % numNodes;
        tr1::unordered_set<int> *visited = new tr1::unordered_set<int>();
        ALLEGRO_COLOR color = al_map_rgb(rand() % 255, rand() % 255, rand() % 255);
        int steps = traverse(index, nodes, visited, 0, limit, coords, color, visual);
        sum += 1.0 / steps;
    }

    sum *= (numNodes / times);
    cout << "Approximate C: " << sum << endl;

    if(visual) {
        event_loop();
        deinit();
    }

    return 0;
}

void init(int width, int height) {
    al_init();
    display = al_create_display(width, height);
    event_queue = al_create_event_queue();
    al_register_event_source(event_queue, al_get_display_event_source(display));
    al_install_keyboard();
    al_register_event_source(event_queue, al_get_keyboard_event_source());
    al_init_primitives_addon();

    al_clear_to_color(al_map_rgb(254,254,254));
}

void deinit() {
    al_destroy_event_queue(event_queue);
    al_destroy_display(display);
}

void event_loop() {
    while(1) {
        bool have_event = al_wait_for_event_timed(event_queue, &event, 0.05);
        if (have_event) {
            if (event.type == ALLEGRO_EVENT_DISPLAY_CLOSE || event.type == ALLEGRO_EVENT_KEY_UP) break;
        }
   }
}